import path from "path";
import { defineConfig, loadEnv } from "vite";
import react from "@vitejs/plugin-react";

const rootPath = path.resolve(__dirname, "..", "..");
const rootDir = path.resolve(rootPath, "modules");

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };

  return {
    plugins: [react()],
    root: rootPath,
    base: "/",
    resolve: {
      alias: {
        "@public": path.resolve(rootPath, "public"),
        "@assets": path.resolve(rootPath, "assets"),
        "@core": path.resolve(rootDir, "core"),
        "@app": path.resolve(rootDir, "app"),
      },
    },
    envDir: path.resolve(rootPath, ".env"),
    server: {
      port: 8080,
    },
  };
});
