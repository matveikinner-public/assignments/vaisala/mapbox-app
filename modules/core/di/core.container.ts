import { Container } from "inversify";
import coreBindings from "./core.bindings";
import { IHttpService } from "@core/domain";
import { HttpService } from "@core/data";

const container = new Container();

container.bind<IHttpService>(coreBindings.HttpService).to(HttpService).inSingletonScope();

export default container;
