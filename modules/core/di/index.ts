import coreBindings from "./core.bindings";
import coreContainer from "./core.container";

export { coreBindings, coreContainer };
