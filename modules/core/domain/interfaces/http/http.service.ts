import { AxiosInstance } from "axios";

interface IHttpService {
  get httpClient(): AxiosInstance;
}

export default IHttpService;
