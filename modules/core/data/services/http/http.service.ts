import { injectable } from "inversify";
import axios, { AxiosError, AxiosInstance, AxiosRequestConfig } from "axios";
import { IHttpService } from "@core/domain";

@injectable()
class HttpService implements IHttpService {
  private client: AxiosInstance;

  /**
   * Axios configuration options
   *
   * @returns {AxiosRequestConfig} AxiosRequestConfig
   * @docs https://axios-http.com/docs/req_config
   */
  private options: AxiosRequestConfig = {
    baseURL: import.meta.env.VITE_HTTP_BASE_URL,
    timeout: 60000,
  };

  constructor() {
    this.client = axios.create(this.options);

    this.client.interceptors.request.use((req) => {
      if (import.meta.env.MODE === "development") console.debug(req);
      return req;
    });

    this.client.interceptors.response.use(
      (res) => {
        if (import.meta.env.MODE === "development") console.debug(res);
        return res;
      },
      (err: AxiosError) => {
        if (import.meta.env.MODE === "development") console.debug(err.response?.data);
        return Promise.reject(err);
      }
    );
  }

  /**
   * Getter method which returns http client to interact with the API
   * @returns {AxiosInstance} AxiosInstance
   */
  get httpClient(): AxiosInstance {
    if (!this.client) {
      throw Error("Attempt to use Http Service before it was initialized");
    }
    return this.client;
  }
}

export default HttpService;
