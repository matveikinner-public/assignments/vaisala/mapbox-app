export type HttpNetworkResponseModel<T> = {
  statusCode: number;
  data: T;
};
