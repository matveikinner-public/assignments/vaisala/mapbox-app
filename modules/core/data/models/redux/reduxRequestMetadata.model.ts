export type ReduxRequestMetadataStatus = "IDLE" | "PENDING" | "SUCCESS" | "FAILURE";

export type ReduxRequestMetadata<T> = {
  status: ReduxRequestMetadataStatus;
  action: T | "IDLE";
  isLoading: boolean;
  isError: boolean;
  error?: unknown;
};

export const genReduxRequestMetadata = <T>(
  status: ReduxRequestMetadataStatus,
  action: T | "IDLE",
  error?: unknown
): ReduxRequestMetadata<T> => ({
  status,
  action,
  isLoading: status === "PENDING",
  isError: status === "FAILURE",
  error,
});
