import { FunctionComponent } from "react";
import { RouterProvider } from "react-router-dom";
import { I18nextProvider } from "react-i18next";
import { Provider } from "react-redux";
import { CssBaseline, ThemeProvider, createTheme } from "@mui/material";
import { SnackbarProvider } from "notistack";

import store from "./frameworks/redux";
import i18n from "./frameworks/i18next";
import vaisalaTheme from "./frameworks/material-ui";
import notistackProps from "./frameworks/notistack";
import router from "./Routes";

const CoreProvider: FunctionComponent = () => {
  return (
    <Provider store={store}>
      <I18nextProvider i18n={i18n}>
        <ThemeProvider theme={createTheme(vaisalaTheme)}>
          <CssBaseline />
          <SnackbarProvider {...notistackProps}>
            <RouterProvider router={router} />
          </SnackbarProvider>
        </ThemeProvider>
      </I18nextProvider>
    </Provider>
  );
};

export default CoreProvider;
