import { FunctionComponent, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SnackbarKey, closeSnackbar, useSnackbar } from "notistack";
import { IconButton } from "@mui/material";
import { Close as CloseIcon } from "@mui/icons-material";
import { selectToastState, resetToast } from "@core/ui/adapters/redux";

const Toast: FunctionComponent = () => {
  const dispatch = useDispatch();

  const { type, message } = useSelector(selectToastState);

  const { enqueueSnackbar } = useSnackbar();

  const defaultIconAction = (snackbarId: SnackbarKey) => (
    <IconButton aria-label="delete" size="medium" onClick={() => closeSnackbar(snackbarId)} sx={{ color: "white" }}>
      <CloseIcon fontSize="small" />
    </IconButton>
  );

  useEffect(() => {
    if (message) {
      enqueueSnackbar(message, {
        variant: type,
        action: defaultIconAction,
      });

      dispatch(resetToast());
    }
  }, [type, message]);

  return null;
};

export default Toast;
