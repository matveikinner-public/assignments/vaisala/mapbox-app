import { FunctionComponent } from "react";
import { useSelector } from "react-redux";
import { LinearProgress } from "@mui/material";
import { selectProgressState } from "@core/ui/adapters/redux";

const Progress: FunctionComponent = () => {
  const { isActive } = useSelector(selectProgressState);

  if (isActive)
    return (
      <LinearProgress
        sx={{
          position: "fixed",
          zIndex: 1,
          top: 0,
          right: 0,
          left: 0,
          backgroundColor: "red",
          "& .MuiLinearProgress-bar": {
            backgroundColor: "blue",
          },
        }}
      />
    );

  return null;
};

export default Progress;
