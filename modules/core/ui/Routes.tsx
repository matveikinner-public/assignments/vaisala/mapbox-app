import { Outlet, RouteObject, createBrowserRouter } from "react-router-dom";
import appRootRoutes from "@app/ui/Routes";

export const routePaths = {
  root: "/",
  ...appRootRoutes,
  notFound: "*",
} as const;

export type Route = (typeof routePaths)[keyof typeof routePaths];

const routes: RouteObject[] = [
  {
    path: routePaths.root,
    element: <Outlet />,
    children: [...appRootRoutes],
  },
];

const router = createBrowserRouter(routes);

export default router;
