import { all } from "redux-saga/effects";
import appRootSaga from "@app/ui/adapters/redux-saga";

export { exceptionWatcherSaga } from "./shared/exceptionWatcherSaga";

export default function* rootSaga() {
  yield all([appRootSaga()]);
}
