import { put } from "redux-saga/effects";
import { t } from "i18next";
import { isAxiosError } from "axios";
import { createToast, decreaseProgess, increaseProgess } from "@core/ui/adapters/redux";

export const exceptionWatcherSaga = <T>(childrenSaga: (...args: T[]) => Generator) =>
  function* (...args: T[]) {
    yield put(increaseProgess());

    try {
      // Run children Saga(s)
      yield childrenSaga(...args);
    } catch (err: unknown) {
      // 1. Check if the error is Axios error
      if (isAxiosError(err)) {
        if (import.meta.env.MODE === "development")
          console.debug(exceptionWatcherSaga.name, "Axios error in attempt to run Saga generator function");

        // 1.1. Check if the server responds
        if (err.response) {
          yield put(
            createToast({
              type: "error",
              message: t([`shared:error.server.status${err.response.status}`, "shared:error.server.status.unknown"], {
                value: err.response.status,
              }),
            })
          );
        } else {
          yield put(
            createToast({
              type: "error",
              message: t(["shared:error.server.status.down", "shared:error.server.status.unknown"]),
            })
          );
        }
      }

      // 2. Check if the error is application logic error
      else if (err instanceof Error) {
        if (import.meta.env.MODE === "development")
          console.debug(exceptionWatcherSaga.name, "Application error in attempt to run Saga generator function");

        yield put(
          createToast({
            type: "error",
            message: t("shared:error.application.unknown"),
          })
        );
      }

      // 3. Display default error message
      // NOTE: Consider to submit error message to error service in order to log and investigate
      else {
        if (import.meta.env.MODE === "development")
          console.debug(exceptionWatcherSaga.name, "Unknown error in attempt to run Saga generator function");

        yield put(
          createToast({
            type: "error",
            message: t("shared:error.unknown"),
          })
        );
      }
    } finally {
      yield put(decreaseProgess());
    }
  };
