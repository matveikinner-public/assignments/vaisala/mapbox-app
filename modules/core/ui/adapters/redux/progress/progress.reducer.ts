import { createReducer } from "@reduxjs/toolkit";
import { initialState } from "./progress.constants";
import { decreaseProgess, increaseProgess, resetProgess } from "./progress.actions";

const reducer = createReducer(initialState, (builder) =>
  builder
    .addCase(increaseProgess, (state) => ({
      ...state,
      isActive: true,
      tasks: state.activeTasks + 1,
    }))
    .addCase(decreaseProgess, (state) => ({
      ...state,
      isActive: state.activeTasks - 1 > 0,
      tasks: state.activeTasks - 1,
    }))
    .addCase(resetProgess, () => ({
      ...initialState,
    }))
);

export default reducer;
