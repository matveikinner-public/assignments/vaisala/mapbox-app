import { createAction } from "@reduxjs/toolkit";
import { ProgressActionType } from "./progress.types";

export const increaseProgess = createAction<void, ProgressActionType>("increaseProgress");
export const decreaseProgess = createAction<void, ProgressActionType>("decreaseProgress");
export const resetProgess = createAction<void, ProgressActionType>("resetProgress");
