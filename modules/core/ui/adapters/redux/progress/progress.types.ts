export type IncreaseProgressActionType = "increaseProgress";
export type DecreaseProgressActionType = "decreaseProgress";
export type ResetProgressActionType = "resetProgress";

export type ProgressActionType = IncreaseProgressActionType | DecreaseProgressActionType | ResetProgressActionType;

export type ProgressState = {
  isActive: boolean;
  activeTasks: number;
};
