import { ProgressState } from "./progress.types";

export const initialState: ProgressState = {
  isActive: false,
  activeTasks: 0,
};
