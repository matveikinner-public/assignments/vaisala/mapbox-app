import { RootState } from "@core/ui/frameworks/redux";
import { ProgressState } from "./progress.types";

export const selectProgressState = (state: RootState): ProgressState => state.core.progressSlice;
