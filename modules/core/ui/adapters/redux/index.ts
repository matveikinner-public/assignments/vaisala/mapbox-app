import { combineReducers } from "@reduxjs/toolkit";
import progressReducer from "./progress/progress.reducer";
import toastReducer from "./toast/toast.reducer";

export default combineReducers({
  progressSlice: progressReducer,
  toastSlice: toastReducer,
});

export * from "./progress/progress.actions";
export * from "./progress/progress.selectors";

export * from "./toast/toast.actions";
export * from "./toast/toast.selectors";
