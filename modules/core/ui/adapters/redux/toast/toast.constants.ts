import { ToastState } from "./toast.types";

export const initialState: ToastState = {
  type: "info",
  message: "",
};
