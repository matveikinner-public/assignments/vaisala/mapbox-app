import { RootState } from "@core/ui/frameworks/redux";
import { ToastState } from "./toast.types";

export const selectToastState = (state: RootState): ToastState => state.core.toastSlice;
