import { AlertProps } from "@mui/material";

export type CreateToastActionType = "createToast";
export type ResetToastActionType = "resetToast";

export type ToastActionType = CreateToastActionType | ResetToastActionType;

export type ToastState = {
  type: AlertProps["severity"];
  message: string;
};
