import { createReducer } from "@reduxjs/toolkit";
import { createToast, resetToast } from "./toast.actions";
import { initialState } from "./toast.constants";

const reducer = createReducer(initialState, (builder) =>
  builder
    .addCase(createToast, (_, { payload }) => ({
      ...payload,
    }))
    .addCase(resetToast, () => ({
      ...initialState,
    }))
);

export default reducer;
