import { createAction } from "@reduxjs/toolkit";
import { ToastActionType, ToastState } from "./toast.types";

export const createToast = createAction<ToastState, ToastActionType>("createToast");
export const resetToast = createAction<void, ToastActionType>("resetToast");
