import "@mui/material";

declare module "@mui/material/styles" {
  interface Palette {
    vaisalaTheme: Palette["primary"];
  }

  interface PaletteOptions {
    vaisalaTheme?: PaletteOptions["primary"];
  }

  interface PaletteColor {
    highlight?: string;
  }

  interface SimplePaletteColorOptions {
    highlight?: string;
  }
}
