import { ThemeOptions } from "@mui/material";
import { globalPalette } from "./palette/global.palette";

const vaisalaTheme: ThemeOptions = {
  palette: globalPalette,
};

export default vaisalaTheme;
