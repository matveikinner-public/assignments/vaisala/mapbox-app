import { ThemeOptions } from "@mui/material";

const vaisalaThemeBaseMainColor = "#313131";

export const globalPalette: ThemeOptions["palette"] = {
  vaisalaTheme: {
    main: vaisalaThemeBaseMainColor,
  },
};
