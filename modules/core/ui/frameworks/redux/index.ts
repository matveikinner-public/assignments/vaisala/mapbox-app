import { configureStore, combineReducers } from "@reduxjs/toolkit";
import rootSaga from "@core/ui/adapters/redux-saga";
import sagaMiddleware from "../redux-saga";

import coreRootReducer from "@core/ui/adapters/redux";
import appRootReducer from "@app/ui/adapters/redux";

const rootReducer = combineReducers({
  core: coreRootReducer,
  app: appRootReducer,
});

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }).concat(sagaMiddleware),
  devTools: import.meta.env.MODE === "development",
});

sagaMiddleware.run(rootSaga);

export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;

export default store;
