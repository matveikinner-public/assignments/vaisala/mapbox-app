import dashboardLayout_FI_FI from "./layouts/dashboardLayout.json";

import error_FI_FI from "./shared/error.json";
import notifications_FI_FI from "./shared/notifications.json";

const layouts = {
  dashboardLayout: dashboardLayout_FI_FI,
};

const shared = {
  error: error_FI_FI,
  notifications: notifications_FI_FI,
};

export const Locales_FI_FI = {
  layouts,
  shared,
};
