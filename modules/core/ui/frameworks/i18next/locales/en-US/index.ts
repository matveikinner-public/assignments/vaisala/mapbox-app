import dashboardLayout_EN_US from "./layouts/dashboardLayout.json";

import error_EN_US from "./shared/error.json";
import notifications_EN_US from "./shared/notifications.json";

const layouts = {
  dashboardLayout: dashboardLayout_EN_US,
};

const shared = {
  error: error_EN_US,
  notifications: notifications_EN_US,
};

export const Locales_EN_US = {
  layouts,
  shared,
};
