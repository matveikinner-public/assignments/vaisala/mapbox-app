import i18n, { InitOptions } from "i18next";
import LanguageDetector, { DetectorOptions } from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";

import { Locales_FI_FI } from "./locales/fi-FI";
import { Locales_EN_US } from "./locales/en-US";

export const resources = {
  en_US: {
    ...Locales_EN_US,
  },
  fi_FI: {
    ...Locales_FI_FI,
  },
};

export type AvailableLocales = keyof typeof resources;

const fallbackLng: AvailableLocales = "en_US";

const detection: DetectorOptions = {};

const parseMissingKeyHandler: InitOptions["parseMissingKeyHandler"] = (_key: string, _defaultValue?: string) =>
  "__i18n__";

export const defaultNS: AvailableLocales = "en_US";

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({ detection, interpolation: { escapeValue: false }, fallbackLng, defaultNS, resources, parseMissingKeyHandler })
  .then(() => {
    console.log("Success in attempt to initialize translations with i18next");
  })
  .catch((err) => {
    console.error("Error in attempt to initialize translations with i18next", err);
  });

export default i18n;
