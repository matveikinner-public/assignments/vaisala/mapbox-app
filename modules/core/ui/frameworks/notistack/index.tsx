import { MaterialDesignContent, SnackbarProviderProps } from "notistack";
import { styled } from "@mui/material";
import {
  CheckCircle as CheckCircleIcon,
  Error as ErrorIcon,
  Info as InfoIcon,
  Warning as WarningIcon,
} from "@mui/icons-material";

const StyledMaterialDesignContentSuccess = styled(MaterialDesignContent)(() => ({
  zIndex: 2001,
  "&.notistack-MuiContent-success": {
    // backgroundColor: "#2D7738"
  },
}));

const StyledMaterialDesignContentError = styled(MaterialDesignContent)(() => ({
  zIndex: 2001,
  "&.notistack-MuiContent-error": {
    // backgroundColor: "#970C0C"
  },
}));

const StyledMaterialDesignContentWarning = styled(MaterialDesignContent)(() => ({
  zIndex: 2001,
  "&.notistack-MuiContent-warning": {
    // backgroundColor: "#970C0C"
  },
}));

const StyledMaterialDesignContentInfo = styled(MaterialDesignContent)(() => ({
  zIndex: 2001,
  "&.notistack-MuiContent-info": {
    // backgroundColor: "#970C0C"
  },
}));

const notistackProps: SnackbarProviderProps = {
  maxSnack: 3,
  anchorOrigin: { vertical: "bottom", horizontal: "center" },
  preventDuplicate: true,
  iconVariant: {
    success: <CheckCircleIcon sx={{ mr: 1 }} />,
    error: <ErrorIcon sx={{ mr: 1 }} />,
    warning: <WarningIcon sx={{ mr: 1 }} />,
    info: <InfoIcon sx={{ mr: 1 }} />,
  },
  Components: {
    success: StyledMaterialDesignContentSuccess,
    error: StyledMaterialDesignContentError,
    warning: StyledMaterialDesignContentWarning,
    info: StyledMaterialDesignContentInfo,
  },
};

export default notistackProps;
