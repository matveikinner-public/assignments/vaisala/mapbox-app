import appBindings from "./app.bindings";
import appContainer from "./app.container";

export { appBindings, appContainer };
