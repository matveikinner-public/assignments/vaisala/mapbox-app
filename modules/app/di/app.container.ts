import { Container } from "inversify";
import { coreContainer } from "@core/di";
import { GetLocationsUseCase, LocationRepository, UploadLocationFileUseCase } from "@app/domain";
import { LocationApi, LocationRepositoryImpl, RemoteLocationRepository } from "@app/data";
import appBindings from "./app.bindings";

const container = new Container();

container.parent = coreContainer;

// ---------------------------------------------------------------------------------------------------------------------
// Location
// ---------------------------------------------------------------------------------------------------------------------
container.bind<LocationRepository>(appBindings.LocationRepository).to(LocationRepositoryImpl);
container.bind<LocationRepository>(appBindings.RemoteLocationRepository).to(RemoteLocationRepository);
container.bind<LocationApi>(appBindings.LocationApi).to(LocationApi);

container.bind<GetLocationsUseCase>(appBindings.GetLocationsUseCase).to(GetLocationsUseCase);
container.bind<UploadLocationFileUseCase>(appBindings.UploadLocationFileUseCase).to(UploadLocationFileUseCase);

export default container;
