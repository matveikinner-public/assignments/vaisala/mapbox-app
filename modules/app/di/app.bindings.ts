export default {
  LocationRepository: Symbol.for("LocationRepository"),
  RemoteLocationRepository: Symbol.for("RemoteLocationRepository"),
  LocationApi: Symbol.for("LocationApi"),
  GetLocationsUseCase: Symbol.for("GetLocationsUseCase"),
  UploadLocationFileUseCase: Symbol.for("UploadLocationFileUseCase"),
};
