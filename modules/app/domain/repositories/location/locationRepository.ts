import { LocationModel, LocationQuery } from "@app/domain/models";
import { Observable } from "rxjs";

interface LocationRepository {
  getLocations(query?: LocationQuery): Observable<LocationModel[]>;
  uploadFile(input: FormData, query?: LocationQuery): Observable<LocationModel[]>;
}

export default LocationRepository;
