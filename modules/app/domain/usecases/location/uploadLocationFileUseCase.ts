import { inject, injectable } from "inversify";
import { appBindings } from "@app/di";
import { type LocationRepository } from "@app/domain";
import { LocationQuery } from "@app/domain/models";

@injectable()
class UploadLocationFileUseCase {
  @inject(appBindings.LocationRepository) private locationRepository!: LocationRepository;

  invoke = (input: FormData, query?: LocationQuery) => this.locationRepository.uploadFile(input, query);
}

export default UploadLocationFileUseCase;
