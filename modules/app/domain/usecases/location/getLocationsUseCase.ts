import { inject, injectable } from "inversify";
import { appBindings } from "@app/di";
import { type LocationRepository } from "@app/domain";
import { LocationQuery } from "@app/domain/models";

@injectable()
class GetLocationsUseCase {
  @inject(appBindings.LocationRepository) private locationRepository!: LocationRepository;

  invoke = (query?: LocationQuery) => this.locationRepository.getLocations(query);
}

export default GetLocationsUseCase;
