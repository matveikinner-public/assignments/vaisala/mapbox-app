export const locationTempUnits = ["celsius", "fahrenheit"] as const;

export type LocationTempUnitType = (typeof locationTempUnits)[number];
