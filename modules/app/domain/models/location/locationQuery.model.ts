import { LocationTempUnitType } from "./locationTemp.model";

export type LocationQuery = {
  tempUnit: LocationTempUnitType;
};
