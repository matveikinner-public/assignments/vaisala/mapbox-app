import { all, takeLatest } from "redux-saga/effects";
import { exceptionWatcherSaga } from "@core/ui/adapters/redux-saga";
import uploadLocationFileRequestSaga from "./location/uploadLocationFileRequestSaga";
import getLocationsRequestSaga from "./location/getLocationsRequestSaga";
import { getLocationsRequest, uploadLocationFileRequest } from "../redux";

function* appRootSaga() {
  yield all([
    takeLatest(getLocationsRequest.type, exceptionWatcherSaga(getLocationsRequestSaga)),
    takeLatest(uploadLocationFileRequest.type, exceptionWatcherSaga(uploadLocationFileRequestSaga)),
  ]);
}

export default appRootSaga;
