import { put } from "redux-saga/effects";
import { PayloadAction } from "@reduxjs/toolkit";
import { t } from "i18next";
import { Observable, lastValueFrom } from "rxjs";
import { createToast } from "@core/ui/adapters/redux";
import { appBindings, appContainer } from "@app/di";
import { UploadLocationFileUseCase } from "@app/domain";
import { LocationModel, LocationQuery } from "@app/domain/models";
import { uploadLocationFileFailure, uploadLocationFileSuccess } from "../../redux";

function* uploadLocationFileRequestSaga({
  payload: { input, query },
}: PayloadAction<{ input: FormData; query?: LocationQuery }>) {
  const useCase = appContainer.get<UploadLocationFileUseCase>(appBindings.UploadLocationFileUseCase);

  try {
    const response = (yield lastValueFrom(
      (yield useCase.invoke(input, query)) as Observable<LocationModel[]>
    )) as LocationModel[];

    yield put(
      createToast({
        type: "success",
        message: t("shared:notifications.redux-saga.location.uploadLocationFileRequestSaga.success"),
      })
    );

    yield put(uploadLocationFileSuccess(response));
  } catch (err) {
    yield put(
      createToast({
        type: "error",
        message: t("shared:notifications.redux-saga.location.uploadLocationFileRequestSaga.failure"),
      })
    );

    yield put(uploadLocationFileFailure(err));
    throw err;
  }
}

export default uploadLocationFileRequestSaga;
