import { put } from "redux-saga/effects";
import { PayloadAction } from "@reduxjs/toolkit";
import { t } from "i18next";
import { Observable, lastValueFrom } from "rxjs";
import { createToast } from "@core/ui/adapters/redux";
import { appBindings, appContainer } from "@app/di";
import { GetLocationsUseCase } from "@app/domain";
import { LocationModel, LocationQuery } from "@app/domain/models";
import { getLocationsFailure, getLocationsSuccess } from "../../redux";

function* getLocationsRequestSaga({ payload: { query } }: PayloadAction<{ query: LocationQuery }>) {
  const useCase = appContainer.get<GetLocationsUseCase>(appBindings.GetLocationsUseCase);

  try {
    const response = (yield lastValueFrom(
      (yield useCase.invoke(query)) as Observable<LocationModel[]>
    )) as LocationModel[];

    yield put(
      createToast({
        type: "success",
        message: t("shared:notifications.redux-saga.location.getLocationsRequestSaga.success"),
      })
    );

    yield put(getLocationsSuccess(response));
  } catch (err) {
    yield put(
      createToast({
        type: "error",
        message: t("shared:notifications.redux-saga.location.getLocationsRequestSaga.failure"),
      })
    );

    yield put(getLocationsFailure(err));
    throw err;
  }
}

export default getLocationsRequestSaga;
