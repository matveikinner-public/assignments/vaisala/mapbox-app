import { combineReducers } from "@reduxjs/toolkit";
import locationReducer from "./location/location.reducer";

const appRootReducer = combineReducers({
  locationSlice: locationReducer,
});

export default appRootReducer;

export * from "./location/location.actions";
export * from "./location/location.selectors";
