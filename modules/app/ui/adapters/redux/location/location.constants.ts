import { genReduxRequestMetadata } from "@core/data";
import { LocationState } from "./location.types";

export const initialState: LocationState = {
  data: {
    locations: [],
  },
  requests: {
    getLocations: {
      meta: genReduxRequestMetadata("IDLE", "IDLE"),
    },
    uploadFile: {
      meta: genReduxRequestMetadata("IDLE", "IDLE"),
    },
  },
};
