import { RootState } from "@core/ui/frameworks/redux";
import { LocationModel } from "@app/domain/models";
import { LocationState } from "./location.types";

export const selectLocationState = (state: RootState): LocationState => state.app.locationSlice;
export const selectLocations = (state: RootState): LocationModel[] => state.app.locationSlice.data.locations;
