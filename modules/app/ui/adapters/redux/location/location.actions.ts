import { createAction } from "@reduxjs/toolkit";
import { LocationModel, LocationQuery } from "@app/domain/models";
import { LocationActionType } from "./location.types";

export const getLocationsRequest = createAction<{ query?: LocationQuery }, LocationActionType>("getLocationsRequest");
export const getLocationsSuccess = createAction<LocationModel[], LocationActionType>("getLocationsSuccess");
export const getLocationsFailure = createAction<unknown, LocationActionType>("getLocationsFailure");

export const uploadLocationFileRequest = createAction<{ input: FormData; query?: LocationQuery }, LocationActionType>(
  "uploadLocationFileRequest"
);
export const uploadLocationFileSuccess = createAction<LocationModel[], LocationActionType>("uploadLocationFileSuccess");
export const uploadLocationFileFailure = createAction<unknown, LocationActionType>("uploadLocationFileFailure");
