import { createReducer } from "@reduxjs/toolkit";
import { initialState } from "./location.constants";
import {
  getLocationsFailure,
  getLocationsRequest,
  getLocationsSuccess,
  uploadLocationFileFailure,
  uploadLocationFileRequest,
  uploadLocationFileSuccess,
} from "./location.actions";
import { genReduxRequestMetadata } from "@core/data";

const reducer = createReducer(initialState, (builder) =>
  builder
    .addCase(getLocationsRequest, (state) => ({
      ...state,
      requests: {
        ...state.requests,
        getLocations: {
          meta: genReduxRequestMetadata("PENDING", getLocationsRequest.type),
        },
      },
    }))
    .addCase(getLocationsSuccess, (state, { payload }) => ({
      ...state,
      data: {
        ...state.data,
        locations: payload,
      },
      requests: {
        ...state.requests,
        getLocations: {
          meta: genReduxRequestMetadata("SUCCESS", getLocationsSuccess.type),
        },
      },
    }))
    .addCase(getLocationsFailure, (state, { payload: error }) => ({
      ...state,
      requests: {
        ...state.requests,
        getLocations: {
          meta: genReduxRequestMetadata("FAILURE", getLocationsFailure.type, error),
        },
      },
    }))
    //
    .addCase(uploadLocationFileRequest, (state) => ({
      ...state,
      requests: {
        ...state.requests,
        uploadFiles: {
          meta: genReduxRequestMetadata("PENDING", uploadLocationFileRequest.type),
        },
      },
    }))
    .addCase(uploadLocationFileSuccess, (state, { payload }) => ({
      ...state,
      data: {
        ...state.data,
        locations: payload,
      },
      requests: {
        ...state.requests,
        uploadFiles: {
          meta: genReduxRequestMetadata("SUCCESS", uploadLocationFileSuccess.type),
        },
      },
    }))
    .addCase(uploadLocationFileFailure, (state, { payload: error }) => ({
      ...state,
      requests: {
        ...state.requests,
        uploadFiles: {
          meta: genReduxRequestMetadata("FAILURE", uploadLocationFileFailure.type, error),
        },
      },
    }))
);

export default reducer;
