import { ReduxRequestMetadata } from "@core/data";
import { LocationModel } from "@app/domain/models";

export type GetLocationsType = "getLocationsRequest" | "getLocationsSuccess" | "getLocationsFailure";

export type UploadLocationFileType =
  | "uploadLocationFileRequest"
  | "uploadLocationFileSuccess"
  | "uploadLocationFileFailure";

export type LocationActionType = GetLocationsType | UploadLocationFileType;

export type LocationActionMetadata = ReduxRequestMetadata<LocationActionType | "IDLE">;

export type LocationState = {
  data: {
    locations: LocationModel[];
  };
  requests: {
    getLocations: {
      meta: LocationActionMetadata;
    };
    uploadFile: {
      meta: LocationActionMetadata;
    };
  };
};
