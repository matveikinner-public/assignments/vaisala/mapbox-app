import { Suspense, lazy } from "react";
import { RouteObject } from "react-router-dom";

import { DashboardLayout } from "./layouts";

const DashboardPage = lazy(() => import("./pages/dashboard/DashboardPage"));

export const appRoutePaths = {
  appRoot: "/",
  dashboard: "/",
} as const;

const dashboardRoutes: RouteObject[] = [
  {
    path: appRoutePaths.dashboard,
    element: (
      <Suspense fallback={<div />}>
        <DashboardPage />
      </Suspense>
    ),
  },
];

const appRootRoutes: RouteObject[] = [
  {
    path: appRoutePaths.appRoot,
    element: <DashboardLayout />,
    children: [...dashboardRoutes],
  },
];

export default appRootRoutes;
