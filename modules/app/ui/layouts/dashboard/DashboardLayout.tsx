import { ChangeEvent, FunctionComponent, useState } from "react";
import { Outlet } from "react-router-dom";
import { Namespace } from "i18next";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import {
  AppBar,
  Box,
  Button,
  Drawer,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Stack,
  Toolbar,
  Typography,
} from "@mui/material";
import { CloudUpload as CloudUploadIcon, Menu as MenuIcon } from "@mui/icons-material";
import { Progress, Toast } from "@core/ui/components";
import { getLocationsRequest, uploadLocationFileRequest } from "@app/ui/adapters/redux";
import { LocationTempUnitType, locationTempUnits } from "@app/domain/models";
import { VisuallyHiddenInput } from "./DashboardLayout.styles";

const DashboardLayout: FunctionComponent = () => {
  const I18N_NAMESPACE: Namespace = "layouts";
  const { t } = useTranslation(I18N_NAMESPACE);

  const dispatch = useDispatch();

  const [isMobileOpen, setIsMobileOpen] = useState(false);

  const [tempUnit, setTempUnit] = useState<LocationTempUnitType>("celsius");

  const drawerWidth = 240;

  const handleChangeTemperatureUnit = (event: SelectChangeEvent) => {
    const newTempUnit = event.target.value as LocationTempUnitType;
    setTempUnit(newTempUnit);
    dispatch(getLocationsRequest({ query: { tempUnit: newTempUnit } }));
  };

  const handleOnChangeFile = (event: ChangeEvent<HTMLInputElement>) => {
    const formData = new FormData();
    const file = event.target?.files?.[0];

    if (file) {
      formData.append("file", file);
      dispatch(uploadLocationFileRequest({ input: formData, query: { tempUnit } }));

      event.target.value = "";
    }
  };

  const drawer = (
    <>
      <Box sx={{ py: 4, mx: "auto" }}>
        <img src="https://www.vaisala.com/themes/custom/vaisala/favicon.ico" />
      </Box>
      <Stack alignItems="center" spacing={2}>
        <FormControl sx={{ m: 1, minWidth: 120 }}>
          <InputLabel id="temperature-unit-select-helper-label">
            {t("dashboardLayout.text.input.select.label")}
          </InputLabel>
          <Select
            labelId="temperature-unit-select-helper-label"
            id="temperature-unit-select-helper"
            value={tempUnit}
            label="Temperature"
            onChange={handleChangeTemperatureUnit}
            sx={{ textTransform: "capitalize" }}
          >
            {locationTempUnits.map((item) => (
              <MenuItem key={item} value={item} sx={{ textTransform: "capitalize" }}>
                {item}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <Button component="label" variant="contained" startIcon={<CloudUploadIcon />}>
          {t("dashboardLayout.text.input.file.label")}
          <VisuallyHiddenInput
            id="file-input"
            type="file"
            accept="application/json"
            multiple={false}
            onChange={handleOnChangeFile}
          />
        </Button>
      </Stack>
    </>
  );

  return (
    <Box sx={{ display: "flex", flex: 1 }}>
      <AppBar position="fixed" elevation={0} sx={{ zIndex: 9999 }}>
        <Progress />
        <Toolbar sx={{ backgroundColor: "vaisalaTheme.main" }}>
          <Stack direction="row" alignItems="center" width="100%">
            <IconButton
              onClick={() => setIsMobileOpen((prevValue) => !prevValue)}
              sx={{
                display: { md: "none" },
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="body2">{t("dashboardLayout.text.title")}</Typography>
          </Stack>
        </Toolbar>
      </AppBar>
      <Grid container justifyContent="space-around" width="100%">
        <Grid item component="nav" xs={12} md={3} aria-label="navigation">
          <Drawer
            disableScrollLock
            variant="temporary"
            open={isMobileOpen}
            onClose={() => setIsMobileOpen((prevValue) => !prevValue)}
            ModalProps={{
              keepMounted: true,
            }}
            PaperProps={{
              sx: { pt: "100px" },
            }}
            sx={{
              display: { xs: "block", md: "none" },
              "& .MuiDrawer-paper": { boxSizing: "border-box", width: drawerWidth },
            }}
          >
            {drawer}
          </Drawer>
          <Drawer
            variant="permanent"
            open
            PaperProps={{
              sx: { top: "100px", border: "none", width: { md: "25%", xl: "20%" } },
            }}
            sx={{ display: { xs: "none", md: "flex" } }}
          >
            {drawer}
          </Drawer>
        </Grid>
        <Grid
          item
          component="main"
          xs={12}
          md={9}
          xl={10}
          sx={{
            minHeight: `calc(100% - 100px)`,
            mt: ``,
          }}
        >
          <Outlet />
          <Toast />
        </Grid>
      </Grid>
    </Box>
  );
};

export default DashboardLayout;
