import { FunctionComponent } from "react";
import Map, { Marker } from "react-map-gl";
import { useSelector } from "react-redux";
import { Box, Tooltip } from "@mui/material";
import { selectLocations } from "@app/ui/adapters/redux";
import { pulseAnimation } from "./DashboardPage.styles";

import "mapbox-gl/dist/mapbox-gl.css";

const DashboardPage: FunctionComponent = () => {
  const locations = useSelector(selectLocations);

  return (
    <Map
      mapboxAccessToken={import.meta.env.VITE_MAPBOX_ACCESS_TOKEN}
      initialViewState={{
        longitude: 24.9421,
        latitude: 60.1676,
        zoom: 12,
      }}
      style={{ width: "100vw", height: "100vh" }}
      mapStyle="mapbox://styles/mapbox/dark-v11"
    >
      {locations.map(({ city, lat, lon, temp, tempUnit }) => (
        <Marker key={city} longitude={lon} latitude={lat} anchor="bottom">
          <Tooltip title={`Temperature: ${temp} (${tempUnit})`} arrow placement="top">
            <Box
              sx={{
                height: "16px",
                width: "16px",
                backgroundColor: "red",
                borderRadius: "50%",
                animation: `${pulseAnimation} 1s infinite alternate`,
              }}
            />
          </Tooltip>
        </Marker>
      ))}
    </Map>
  );
};

export default DashboardPage;
