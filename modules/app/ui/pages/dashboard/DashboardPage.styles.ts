import { keyframes } from "@emotion/react";

export const pulseAnimation = keyframes`
    0% {
      // box-shadow: 0 0 0 0px rgba(255, 0, 0, 0.2);
        transform: scale(0.5);
        opacity: 0.5;
    }
    100% {
      // box-shadow: 0 0 0 20px rgba(255, 0, 0, 0);
        transform: scale(2);
        opacity: 0;
    }
  `;
