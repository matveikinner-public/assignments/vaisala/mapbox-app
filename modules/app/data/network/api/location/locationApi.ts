import { inject, injectable } from "inversify";
import { coreBindings } from "@core/di";
import { HttpService } from "@core/data";
import { Observable, from, map, tap } from "rxjs";
import { GetLocationsNetworkResponseModel, UploadLocationFileNetworkResponseModel } from "../../models";
import { LocationQuery } from "@app/domain/models";

@injectable()
class LocationApi {
  @inject(coreBindings.HttpService) private httpService!: HttpService;

  getLocations = (query?: LocationQuery): Observable<GetLocationsNetworkResponseModel> =>
    from(
      this.httpService.httpClient.get<GetLocationsNetworkResponseModel>("/api/v1/locations", {
        params: query,
      })
    ).pipe(
      tap((response) => {
        if (import.meta.env.MODE === "development") console.log(LocationApi.name, response);
      }),
      map(({ data }) => data)
    );

  uploadFile = (input: FormData, query?: LocationQuery): Observable<UploadLocationFileNetworkResponseModel> =>
    from(
      this.httpService.httpClient.post<UploadLocationFileNetworkResponseModel>("/api/v1/locations/uploads", input, {
        params: query,
      })
    ).pipe(
      tap((response) => {
        if (import.meta.env.MODE === "development") console.log(LocationApi.name, response);
      }),
      map(({ data }) => data)
    );
}

export default LocationApi;
