import { HttpNetworkResponseModel } from "@core/data/models";
import { LocationNetworkModel } from "../locationNetwork.model";

export type GetLocationsNetworkResponseModel = HttpNetworkResponseModel<LocationNetworkModel[]>;
