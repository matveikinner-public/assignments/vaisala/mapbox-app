import { HttpNetworkResponseModel } from "@core/data/models";
import { LocationNetworkModel } from "../locationNetwork.model";

export type UploadLocationFileNetworkResponseModel = HttpNetworkResponseModel<LocationNetworkModel[]>;
