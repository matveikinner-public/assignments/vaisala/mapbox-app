export type LocationNetworkModel = {
  id: string;
  city: string;
  lat: number;
  lon: number;
  temp: number;
  tempUnit: string;
  createdAt: string;
  updatedAt: string;
};
