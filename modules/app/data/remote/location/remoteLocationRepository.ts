import { inject, injectable } from "inversify";
import { Observable, map } from "rxjs";
import { LocationRepository } from "@app/domain";
import { appBindings } from "@app/di";
import { LocationApi } from "@app/data";
import { LocationModel, LocationQuery } from "@app/domain/models";

@injectable()
class RemoteLocationRepository implements LocationRepository {
  @inject(appBindings.LocationApi) private locationApi!: LocationApi;

  getLocations = (query?: LocationQuery): Observable<LocationModel[]> =>
    this.locationApi.getLocations(query).pipe(map(({ data }) => data));

  uploadFile = (input: FormData, query?: LocationQuery): Observable<LocationModel[]> =>
    this.locationApi.uploadFile(input, query).pipe(map(({ data }) => data));
}

export default RemoteLocationRepository;
