import { inject, injectable } from "inversify";
import { type LocationRepository } from "@app/domain";
import { appBindings } from "@app/di";
import { LocationQuery } from "@app/domain/models";

@injectable()
class LocationRepositoryImpl implements LocationRepository {
  @inject(appBindings.RemoteLocationRepository) private remoteLocationRepository!: LocationRepository;

  getLocations = (query?: LocationQuery) => this.remoteLocationRepository.getLocations(query);

  uploadFile = (input: FormData, query?: LocationQuery) => this.remoteLocationRepository.uploadFile(input, query);
}

export default LocationRepositoryImpl;
