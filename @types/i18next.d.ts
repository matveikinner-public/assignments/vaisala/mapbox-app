import "i18next";
import { defaultNS, resources } from "../modules/core/ui/frameworks/i18next";

declare module "i18next" {
  interface CustomTypeOptions {
    defaultNS: typeof defaultNS;
    resources: (typeof resources)["en_US"];
  }
}
